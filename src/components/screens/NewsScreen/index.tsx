import { Table, Button } from "react-bootstrap"
import { connect } from "react-redux";
import { useEffect, useState } from "react";
import {LinkContainer} from 'react-router-bootstrap';

import actions from '../../../redux/actions';

import Channel from "../../../domain/model/entities/Channel";
import User from "../../../domain/model/entities/User";
import News from "../../../domain/model/entities/News";

import PaginationProperties from "../../../domain/model/dto/PaginationProperties";

import NewsRow from './NewsRow';
import ChannelFilter from '../../widgets/ChannelFilter';
import ConfirmDialog from "../../dialogs/ConfirmDialog";
import TablePagination from "../../widgets/TablePagination";
import Statistics from "../../../domain/model/dto/Statistics";

type NewsScreenProps = {
    channels: Channel[];
    news: News[];
    user: User;
    statisticsNews: Statistics;
    fetchRequestChannels(): any;
    fetchNewsStatistics(): any;
    deleteNewsRequest(news: News): any;
    fetchNews(channels: Channel[], pagination?: PaginationProperties): any;
};

const NewsScreen  = function(props: NewsScreenProps) {

    const ITEMS_PER_PAGE = 10;
    const visiblePageCount = 10;

    const {news, channels} = props;

    const [showDeleteDialog, setShowDeleteDialog] = useState(false);
    const [currentNews, setCurrentNews] = useState<News>();

    const [filter, setFilter] = useState<Channel[]>([]);

    const [activePage, setActivePage] = useState<number>(1);

    const pageCount = Math.max(Math.ceil(props.statisticsNews.count / ITEMS_PER_PAGE), 1);

    useEffect(()=>{
        props.fetchRequestChannels();
        props.fetchNews(filter, {skip: (activePage -1) * ITEMS_PER_PAGE, count: ITEMS_PER_PAGE});
        props.fetchNewsStatistics();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [activePage]);

    const onClickDeleteNews = (news: News) => {
        setShowDeleteDialog(true);
        setCurrentNews(news);
    };

    const onCloseConfirmDeleteNews = (confirm: boolean) => {
        setShowDeleteDialog(false);
        if(currentNews && confirm){
            props.deleteNewsRequest(currentNews);
        }
    };

    const onUpdateFilter = (filter: Channel[]) => {
        setFilter(filter);
        props.fetchNews(filter);
    }

    const onSelectPage = (page: number) => {
        setActivePage(page);
    }

    return (<div>
            <ConfirmDialog 
                title="Подтверждение удаления новости." 
                text="Удалить новость?" 
                show={showDeleteDialog} 
                onClose={onCloseConfirmDeleteNews}
            />
            <ChannelFilter
                buttonCaption="Добавить канал в отбор"
                placeholder="Отбор по каналам" 
                channels={channels} 
                onUpdateChannels={onUpdateFilter}
                filter={filter}
            />
            <LinkContainer to="/news/add">
                <Button variant="success">Добавить</Button>
            </LinkContainer>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Новость</th>
                        <th>Каналы</th>
                        <th>Дата публикации</th>
                        <th>Дата окончания публикации</th>
                    </tr>
                </thead>
                <tbody>
                    {news.map((value, index) => {
                        return (<NewsRow key={index} news={value} index={index} onClickDelete={onClickDeleteNews}/>);
                    })}
                </tbody>
            </Table>
            <TablePagination 
                activePage={activePage} 
                visiblePageCount={visiblePageCount} 
                maxPage={pageCount} 
                onSelectPage={onSelectPage}
            />
        </div>);
}

function mapStateToProps(state: any) {
    return {
      channels: state.channels,
      user: state.users,
      news: state.news,
      statisticsNews: state.statisticsNews
    };
}

export default connect(mapStateToProps, actions)(NewsScreen);