import moment from 'moment';

import { Button } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import News from "../../../domain/model/entities/News";

type NewsRowProps = {
    index: number;
    news: News;
    onClickDelete(news: News): void;
}

export default function NewsRow(props: NewsRowProps) {

    const {index, news} = props;

    return (<tr>
        <td>{index+1}</td>
        <td>{news.caption}</td>
        <td>{news.channels.map(value => value.name).join(', ')}</td>
        <td>{moment(news.postingDate).format('MM.DD.YYYY')}</td>
        <td>{moment(news.finalDate).format('MM.DD.YYYY')}</td>
        <td>
            <LinkContainer to={`/news/${news.id}`}>
                <Button variant="info">Изменить</Button>
            </LinkContainer>
            <Button variant="danger" onClick={ () => props.onClickDelete(news) }>Удалить</Button>
        </td>        
    </tr>);
}