import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useParams, withRouter } from 'react-router-dom';

import { Button, Form } from "react-bootstrap";

import actions from '../../../redux/actions';
import Channel from '../../../domain/model/entities/Channel';

function EditChannelScreen(props: any){

    const channels: Channel[] = props.channels;

    const params: any = useParams();
    const id: string | undefined = params.id;

    const [channel, setChannel] = useState<Channel>();

    useEffect(()=>{
        if(!id){
            setChannel({name: ''});
        }else{
            const stateChannel = channels.find((value)=>value.id === id);
            if(stateChannel){
                setChannel(stateChannel);
            }
            else{
                props.fetchRequestChannel(id);
            }
        }
    }, [id, channels, props])

    const onSubmit = async (event: React.SyntheticEvent) =>{
        props.postChannel(channel);
        props.history.push('/channels');
    }

    function setName(name: string){
        const newChannel: Channel = {id, name};
        setChannel(newChannel);
    }

    return (<div>
    <Form>
        <Form.Group>
            <Form.Label>ID</Form.Label>
            <Form.Control value={channel?.id} readOnly />
            <Form.Label>Наименование</Form.Label>
            <Form.Control value={channel?.name} type="text" placeholder="Наименование" onChange={(event: any)=>setName(event.target.value)}/>
            <Button variant="primary" onClick={onSubmit}>Сохранить</Button>
        </Form.Group>
    </Form>
    </div>);
}

function mapStateToProps(state: any) {
    return {
      user: state.user,
      channels: state.channels
    };
}

export default connect(mapStateToProps, actions)(withRouter(EditChannelScreen));