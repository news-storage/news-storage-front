import { Button } from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap';

import Channel from "../../../domain/model/entities/Channel";

type ChannelRowProps = {
    channel: Channel,
    index: number
    onClickDelete(channel: Channel): void;
}

export default function ChannelRow(props: ChannelRowProps){
    const {index, channel} = props;
    return (<tr>
        <td>{index+1}</td>
        <td>{channel.id}</td>
        <td>{channel.name}</td>
        <td>
            <LinkContainer to={`/channels/${channel.id}`}>
                <Button variant="info">Изменить</Button>
            </LinkContainer>
            <Button variant="danger" onClick={ () => props.onClickDelete(channel) }>Удалить</Button>
        </td>
    </tr>);
}