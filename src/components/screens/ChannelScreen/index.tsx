import { Button, Table } from "react-bootstrap"
import { connect } from "react-redux";
import {LinkContainer} from 'react-router-bootstrap';
import { useEffect, useState } from "react";

import actions from '../../../redux/actions';

import Channel from "../../../domain/model/entities/Channel";

import ChannelRow from "./ChannelRow";
import ConfirmDialog from "../../dialogs/ConfirmDialog";
import TablePagination from "../../widgets/TablePagination";
import Statistics from "../../../domain/model/dto/Statistics";

const ChannelsScreen  = function(props: any) {
    const ITEMS_PER_PAGE = 10;
    const visiblePageCount = 10;

    const channels = props.channels as Channel[];
    const statisticsChannels = props.statisticsChannels as Statistics;

    const [showDeleteDialog, setShowDeleteDialog] = useState(false);
    const [currentChannel, setCurrentChannel] = useState<Channel>();

    const [activePage, setActivePage] = useState<number>(1);

    const pageCount = Math.max(Math.ceil(statisticsChannels.count / ITEMS_PER_PAGE), 1);

    useEffect(()=>{
        props.fetchRequestChannels({skip: (activePage-1)*ITEMS_PER_PAGE, count: ITEMS_PER_PAGE});
        props.fetchChannelStatistics();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [activePage]);

    const onCloseConfirmDeleteChannel = (confirm: boolean) =>{
        setShowDeleteDialog(false);
        if(currentChannel && confirm){
            props.deleteChannelRequest(currentChannel);
        }
    };

    const onClickDeleteChannel = (channel: Channel) => {
        setShowDeleteDialog(true);
        setCurrentChannel(channel);
    };

    const onSelectPage = (page: number) => {
        setActivePage(page);
    }

    return (<div>
        <ConfirmDialog 
            title="Подтверждение удаления канала." 
            text="При удаление канала будут удалены все новости. Продолжить?" 
            show={showDeleteDialog} 
            onClose={onCloseConfirmDeleteChannel}
        />
        <LinkContainer to="/channels/add">
            <Button variant="success">Добавить</Button>
        </LinkContainer>
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>№</th>
                    <th>id</th>
                    <th>Канал</th>
            </tr>
            </thead>
            <tbody>
                {
                    channels.map((channel, index) => {
                        return (<ChannelRow key = {index} channel = {channel} index = {index} onClickDelete = {onClickDeleteChannel} />);
                    })
                }
            </tbody>
        </Table>
        <TablePagination 
            activePage={activePage}
            visiblePageCount={visiblePageCount}
            maxPage={pageCount}
            onSelectPage={onSelectPage}
        />
        </div>);
}

function mapStateToProps(state: any) {
    return {
      channels: state.channels,
      user: state.users,
      statisticsChannels: state.statisticsChannels,
    };
}

export default connect(mapStateToProps, actions)(ChannelsScreen);