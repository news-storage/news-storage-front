import { Alert } from 'react-bootstrap';
import { classVisibility } from '../../../lib/components';

type ErrorProps = {
    errorText: string
}

export default function ValidattionError(props: ErrorProps) {
    const visible = props.errorText.length>0;
    return (<Alert variant='danger' className={classVisibility(visible)}>{props.errorText}</Alert>);
}