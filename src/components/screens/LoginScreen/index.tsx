import { useState } from 'react';
import { connect } from 'react-redux';

import { Button, Form, Modal} from "react-bootstrap";
import { withRouter } from 'react-router-dom';

import actions from '../../../redux/actions';

import { checkUser } from "../../../domain/services/UserService";
import ValidattionError from './ValidationError';

import background from '../../../resources/background.png';

const LoginScreen  = function(props: any) {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const [validationError, setValidationError] = useState('');

    const onSubmit = async (event: React.SyntheticEvent) =>{
        const user = { username, password};
        const isValid = await checkUser(user);
        if(isValid){
            props.saveUser(user);
            props.history.push('/');
        }else{
            console.error('Неверный логин или пароль');
            setValidationError('Неверный логин или пароль');
        }
    };

    return (<div>
        <img src={background}alt="background" height="100%" width="100%" />
        <Modal show={true}>
            <Modal.Body>
                <Form>
                    <Form.Group>
                        <Form.Label>Имя пользователя</Form.Label>
                        <Form.Control type="text" placeholder="username" onChange={(event: any) => setUsername(event.target.value)}/>
                        <Form.Label>Пароль</Form.Label>
                        <Form.Control type="password" placeholder="password" onChange={(event: any) => setPassword(event.target.value)}/>
                        <ValidattionError errorText={ validationError }/>
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={onSubmit}>Войти</Button>
            </Modal.Footer>
        </Modal>
        </div>);
}

function mapStateToProps(state: any) {
    return {
      user: state.user
    };
}

export default connect(mapStateToProps, actions)(withRouter(LoginScreen));