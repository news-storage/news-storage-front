import moment from 'moment';

import { useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { useParams, withRouter } from "react-router-dom";

import Channel from '../../../domain/model/entities/Channel';
import News from "../../../domain/model/entities/News";

import actions from "../../../redux/actions";
import ChannelFilter from '../../widgets/ChannelFilter';

type EditNewsParams = {
    id?: string;
}

type EditNewsProps = {
    channels: Channel[];
    news: News[];
    postNews(news: News): any;
    history: any;
}

function EditNewsScreen(props: any) {

    props as EditNewsProps;

    const newsList: News[] = props.news;
    const {channels} = props;

    const params: EditNewsParams = useParams();
    const id = params.id;

    const [news, setNews] = useState<News>({
        caption: '',
        text: '',
        postingDate: new Date(),
        finalDate: new Date(),
        channels: []
    });

    useEffect(()=>{
        if (!id) {
            setNews({
                caption: '',
                text: '',
                postingDate: new Date(),
                finalDate: new Date(),
                channels: []
            });
        }else{
            const stateNews = newsList.find((value) => value.id === id );
            if (stateNews) {
                setNews(stateNews);
            }else{

            }
        }
    }, [id, newsList]);

    const handlePostingDate = (event: any) =>{
        const value: string = event.target.value;
        if(value === ''){
            return;
        }
        const newNews: News = Object.assign({}, news);
        newNews.postingDate = new Date(value);
        setNews(newNews);
    };

    const handleFinalDate = (event: any) => {
        const value: string = event.target.value;
        if(value === ''){
            return;
        }
        const newNews: News = Object.assign({}, news);
        newNews.finalDate = new Date(value);
        setNews(newNews);
    }

    const handleCaption = (event: any) => {
        const caption: string = event.target.value;
        const copy = Object.assign({}, news);
        copy.caption = caption;
        setNews(copy);
    }

    const handleText = (event: any) => {
        const text: string = event.target.value;
        const copy = Object.assign({}, news);
        copy.text = text;
        setNews(copy);
    }

    const onSubmit = (event: React.SyntheticEvent) => {
        props.postNews(news);
        props.history.push('/news');
    }

    const onUpdateFilter = (filter: Channel[]) => {
        const copy = Object.assign({}, news);
        copy.channels = filter;
        setNews(copy);
    }

    const formatDate = (date: Date) =>{
        return moment(date).format("YYYY-MM-DD");
    }

    return (<div>
        <Form>
            <Form.Group>
                <ChannelFilter
                    buttonCaption="Добавить канал к новости"
                    placeholder="Поиск канала"
                    channels={channels}
                    filter={news.channels}
                    onUpdateChannels={onUpdateFilter}
                />
                <Form.Label>Заголовок</Form.Label>
                <Form.Control value={news?.caption} type="text" placeholder="Заголовок" onChange={handleCaption}/>
                <Form.Label>Текст</Form.Label>
                <Form.Control value={news?.text} type="text" placeholder="Текст" onChange={handleText} as="textarea" rows={3}/>
                <Form.Label>Дата публикации</Form.Label>
                <Form.Control value={formatDate(news?.postingDate)} type="date" onChange={handlePostingDate} />
                <Form.Label>Дата окончания публикации</Form.Label>
                <Form.Control value={formatDate(news?.finalDate)} type="date" onChange={handleFinalDate} />
                <Button variant="primary" onClick={onSubmit}>Сохранить</Button>
            </Form.Group>
        </Form>
    </div>);
}

function mapStateToProps(state: any) {
    return {
      user: state.user,
      news: state.news,
      channels: state.channels
    };
}

export default connect(mapStateToProps, actions)(withRouter(EditNewsScreen));