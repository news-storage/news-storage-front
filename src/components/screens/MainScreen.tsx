import { useEffect } from "react";
import { connect } from "react-redux";

import Statistics from "../../domain/model/dto/Statistics";

import actions from "../../redux/actions";

type MainScreenProps = {
    statisticsChannels: Statistics,
    statisticsNews: Statistics,
    fetchChannelStatistics(): any,
    fetchNewsStatistics(): any
}

const MainScreen  = function(props: any) {

    props as MainScreenProps;

    useEffect(() => {
        props.fetchChannelStatistics();
        props.fetchNewsStatistics();   
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const countChannels = `Всего каналов: ${props.statisticsChannels.count}`;
    const countNews = `Всего новостей: ${props.statisticsNews.count}`;
    return (<div>
        <h1>Я вас категорически приветсвую!</h1>
        <h2>{countChannels}</h2>
        <h2>{countNews}</h2>
        </div>);
}

function mapStateToProps(state: any) {
    return {
      statisticsChannels: state.statisticsChannels,
      statisticsNews: state.statisticsNews
    };
}

export default connect(mapStateToProps, actions)(MainScreen);