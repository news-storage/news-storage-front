import { Navbar, Nav } from 'react-bootstrap';
import { connect } from 'react-redux';

import actions from '../../redux/actions';

import { LinkContainer } from 'react-router-bootstrap';

import LoginForm from './LoginForm';

const Header = function(props: any) {
    return (
        <Navbar bg="primary" variant="dark">
            <Nav className="mr-auto">
                <LinkContainer to="/">
                    <Navbar.Brand>Админка новостей</Navbar.Brand>
                </LinkContainer>
                <LinkContainer to="/channels" onClick={()=>props.fetchRequestChannels()}>
                    <Nav.Link>Каналы</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/news">
                    <Nav.Link>Новости</Nav.Link>
                </LinkContainer>
            </Nav>
            <LoginForm />
        </Navbar>
    );
}

function mapStateToProps(state: any) {
    return {
      channels: state.channels,
      user: state.users
    };
}

export default connect(mapStateToProps, actions)(Header);