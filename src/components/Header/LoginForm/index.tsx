import { Button, Form} from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap';
import { connect } from 'react-redux';

import actions from '../../../redux/actions';

import User from "../../../domain/model/entities/User";

import { classVisibility } from "../../../lib/components";

const LoginForm = function(props: any) {

    const loginVisible = (!props.user);
    const logoutVisible = !loginVisible;

    function userPreview(user: User | null): string {
        const fullname = !user ? 'Anonimous' : user.username; 
        return `Добро пожаловать, ${fullname}`;
    }

    const logout = () => {
        props.cleanUser();
    }

    return (<Form inline>
        <Form.Text className="mr-sm-2">{userPreview(props.user)}</Form.Text>
        <LinkContainer to="/login">
            <Button id="loginButton" variant="outline-light" className={classVisibility(loginVisible)}>Войти</Button>
        </LinkContainer>
        <Button id="logoutButton" variant="outline-light" onClick={logout} className={classVisibility(logoutVisible)}>Выйти</Button>
    </Form>);
}

function mapStateToProps(state: any) {
    return {
      user: state.user
    };
}

export default connect(mapStateToProps, actions)(LoginForm);