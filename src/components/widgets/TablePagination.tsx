import {Pagination} from 'react-bootstrap';

type TablePaginationProps = {
    activePage: number;
    visiblePageCount: number;
    maxPage: number;
    onSelectPage(page: number): void;
};

function generatePageNumbers(startPage: number, visiblePageCount: number){
    const pageNumbers = [];
    let page = startPage;
    for(let i=0; i<visiblePageCount; i++){
        pageNumbers.push(page);
        page = page + 1;
    }
    return pageNumbers;
}

function getMaxPage(startPage: number, visiblePageCount: number, maxPage: number) {
    return Math.min(startPage + visiblePageCount - 1, maxPage);
}

export default function TablePagination(props: TablePaginationProps){

    const maxVisibleCount = getMaxPage(props.activePage, props.visiblePageCount, props.maxPage);

    const pageNumbers = generatePageNumbers(props.activePage, maxVisibleCount);

    const isVisibleMaxPage = (maxVisibleCount < props.maxPage);

    const onFirstPage = () => {
        props.onSelectPage(1);
    }

    const onLastPage = () => {
        props.onSelectPage(props.maxPage);
    }

    const onPrevPage = () => {
        const nextPage = Math.max(1, props.activePage - 1);
        props.onSelectPage(nextPage);
    }

    const onNextPage = () => {
        const nextPage = Math.min(props.maxPage, props.activePage + 1);
        props.onSelectPage(nextPage);
    }

    const onSelectPage = (page: number) => {
        props.onSelectPage(page);
    }

    return (<Pagination>
            <Pagination.First onClick={onFirstPage}/>
            <Pagination.Prev onClick={onPrevPage}/>

            {pageNumbers.map(page => {
                const isActive = (props.activePage === page);
                return (<Pagination.Item key={page} active={isActive} onClick={() => onSelectPage(page)}>{page}</Pagination.Item>);
            })}

            <Pagination.Ellipsis disabled/>
            {isVisibleMaxPage ? (<Pagination.Item onClick={() => onSelectPage(props.maxPage)}>{props.maxPage}</Pagination.Item>) : null}
            <Pagination.Next onClick={onNextPage} />
            <Pagination.Last onClick={onLastPage}/>
        </Pagination>);
}