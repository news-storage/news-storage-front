import { useEffect, useState } from "react";
import { Form, Button, Dropdown} from "react-bootstrap";

import Channel from "../../domain/model/entities/Channel";

import FilterTag from "./FilterTag";

type ChannelFilterProps = {
    buttonCaption: string;
    placeholder: string;
    channels: Channel[];
    onUpdateChannels(channels: Channel[]): any;
    filter?: Channel[];
};

function ChannelFilter(props: ChannelFilterProps){

    const {channels} = props;

    let [channelsList, setChannelsList] = useState<Channel[]>(channels);

    let [channel, setChannel] = useState<Channel>();

    let [channelsFilter, setChannelsFilter] = useState<Set<Channel>>(new Set());

    let [searchText, setSearchText] = useState<string>('');

    useEffect(() => {
        const initialFilter = new Set<Channel>(props.filter || []);
        setChannelsFilter(initialFilter);
    }, [props]);

    const addChannelToFilter = (event: any)=>{
        if(channel){
            const newFilter = new Set(Array.from(channelsFilter));
            newFilter.add(channel);
            updateChannelFilter(newFilter);
        }
    }

    const onSelectChannel = (idChannel: any, event: any)=>{
        const channel = channels.find(value => value.id === idChannel);
        if(channel){
            setChannel(channel);
            setSearchText(channel.name);
        }
    }

    const onDeleteFilter = (filter: Channel)=>{
        const newFilter = new Set(Array.from(channelsFilter));
        newFilter.delete(filter);
        setSearchText('');
        updateChannelFilter(newFilter);
    }

    const updateChannelFilter = (filter: Set<Channel>) => {
        setChannelsFilter(filter);
        props.onUpdateChannels(Array.from(filter));
    }

    const onChangeSearchText = (event: React.ChangeEvent<any>) => {
        const searchText: string = event.target.value;
        const newChannelsList = channels.filter(value => value.name.includes(searchText));
        setChannelsList(newChannelsList);
        setSearchText(searchText);
    }

    return (<div>
        <Form inline>
            <Form.Group>
                <Button onClick={addChannelToFilter}>{props.buttonCaption}</Button>
                <Dropdown onSelect={onSelectChannel}>
                    <Dropdown.Toggle variant="light">
                        <Form.Control type="text" placeholder={props.placeholder} defaultValue="" value={searchText} onChange={onChangeSearchText}></Form.Control>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        {channelsList.map(value => (<Dropdown.Item key={value.id} eventKey={value.id}>{value.name}</Dropdown.Item>))}
                    </Dropdown.Menu>
                </Dropdown>    
            </Form.Group>
        </Form>
        <div>
            {Array.from(channelsFilter).map(value => (<FilterTag key={value.id} filter={value} name={value.name} onClose={onDeleteFilter}/>))}
        </div>
    </div>);
}

export default ChannelFilter;