import { Badge } from "react-bootstrap";

type FilterTagProps = {
    filter: any,
    name: string;
    onClose(filter: any): void;
}

export default function FilterTag(props: FilterTagProps) {
    return (<Badge pill variant="secondary">{` ${props.name} `}
                <Badge variant="light" className="btn btn-secondary btn-sm" onClick={()=>props.onClose(props.filter)}>X</Badge>
            </Badge>);
}