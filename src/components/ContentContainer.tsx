import Header from "./Header";

export default function ContentContainer(props: any){
    return (<div>
            <Header />
            {props.children}
        </div>);
}