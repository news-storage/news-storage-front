import {
    Switch,
    Route
  } from "react-router-dom";

import ChannelsScreen from "../screens/ChannelScreen";
import MainScreen from '../screens/MainScreen';
import NewsScreen from "../screens/NewsScreen";
import LoginScreen from "../screens/LoginScreen";
import EditChannelScreen from "../screens/EditChannelScreen";
import EditNewsScreen from "../screens/EditNewsScreen";
import AuthRoute from "./AuthRoute";
import ContentContainer from "../ContentContainer";

const Main = function() {
    return (
        <Switch>
          <AuthRoute exact path="/">
            <ContentContainer>
              <MainScreen />
            </ContentContainer>
          </AuthRoute>
          <AuthRoute exact path="/news">
            <ContentContainer>
              <NewsScreen />
            </ContentContainer>
          </AuthRoute>
          <AuthRoute exact path="/channels">
            <ContentContainer>
              <ChannelsScreen />
            </ContentContainer>
          </AuthRoute>
          <Route exact path="/login">
            <LoginScreen />
          </Route>
          <AuthRoute exact path="/channels/add">
            <ContentContainer>
              <EditChannelScreen />
            </ContentContainer>
          </AuthRoute>
          <AuthRoute exact path="/channels/:id">
            <ContentContainer>
              <EditChannelScreen />
            </ContentContainer>
          </AuthRoute>
          <AuthRoute exact path="/news/add">
            <ContentContainer>
              <EditNewsScreen />
            </ContentContainer>
          </AuthRoute>
          <AuthRoute exact path="/news/:id">
            <ContentContainer>
              <EditNewsScreen />
            </ContentContainer>
          </AuthRoute>
        </Switch>
    );
}

export default Main;