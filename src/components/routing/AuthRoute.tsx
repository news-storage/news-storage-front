import { Route, withRouter, Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import actions from "../../redux/actions";

import User from "../../domain/model/entities/User";

function AuthRoute(props: any){
    const user = props.user as User | null;
    if(user === null){
        return  (<Redirect to="/login" />);
    }
    return (<Route {...props} />);
}

function mapStateToProps(state: any) {
    return {
      user: state.user
    };
}

export default connect(mapStateToProps, actions)(withRouter(AuthRoute));