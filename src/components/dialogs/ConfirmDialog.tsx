import { Modal, Button } from "react-bootstrap";

type DialogProps = {
    title: string;
    text?: string;
    show: boolean;
    onClose(saving: boolean): void;
};

export default function ConfirmDialog(props: DialogProps){

    const title = props.title;
    const text = (props.text) ? props.text : props.title;
    const show = props.show;

    function onCancel(){
      props.onClose(false);
    }

    function onConfirm() {
      props.onClose(true);
    }

    return(<Modal show={show} onHide={onCancel}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{text}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onCancel}>
            Отмена
          </Button>
          <Button variant="primary" onClick={onConfirm}>
            ОК
          </Button>
        </Modal.Footer>
      </Modal>);
};