import Channel from "../../domain/model/entities/Channel";
import IAction from "./IAction";

import ActionsTypes from "../constants/ActionTypes";
import PaginationProperties from "../../domain/model/dto/PaginationProperties";
import Statistics from "../../domain/model/dto/Statistics";

export function fetchRequestChannels(props?: PaginationProperties): IAction<PaginationProperties | undefined>{
    return { type: ActionsTypes.CHANNELS.FETCH_REQUEST_MANY, payload: props };
}

export function fetchRequestChannelsSucceded(channels: Channel[]): IAction<Channel[]>{
    return { type: ActionsTypes.CHANNELS.FETCH_MANY_SUCCEDED, payload: channels };
}

export function fetchRequestChannel(id: string): IAction<string> {
    return {type: ActionsTypes.CHANNELS.FETCH_REQUEST_ONE, payload: id};
}

export function fetchRequestChannelSucceded(channel: Channel): IAction<Channel> {
    return { type: ActionsTypes.CHANNELS.CHANGE_ONE, payload: channel };
}

export function postChannel(channel: Channel): IAction<Channel>{
    return { type: ActionsTypes.CHANNELS.SAVE_REQUEST_ONE, payload: channel };
}

export function deleteChannelRequest(channel: Channel): IAction<Channel>{
    return {type: ActionsTypes.CHANNELS.DELETE_REQUEST_ONE, payload: channel };
}

export function deleteChannel(channel: Channel): IAction<Channel>{
    return {type: ActionsTypes.CHANNELS.DELETE_ONE, payload: channel};
}

export function findChannelsByName(name: string): IAction<string>{
    return {type: ActionsTypes.CHANNELS.FIND_BY_NAME, payload: name};
}

export function fetchChannelStatistics(): IAction<null>{
    return {type: ActionsTypes.CHANNELS.FETCH_STATISTICS, payload: null};
}

export function saveChannelStatistics(statisitcs: Statistics): IAction<Statistics>{
    return {type: ActionsTypes.CHANNELS.SAVE_STATISTICS, payload: statisitcs};
}