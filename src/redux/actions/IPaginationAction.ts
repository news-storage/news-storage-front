import PaginationProperties from "../../domain/model/dto/PaginationProperties";
import IAction from "./IAction";

export default interface IPaginationAction<T = any> extends IAction<T>{
    pagination?: PaginationProperties;
}