import Channel from "../../domain/model/entities/Channel";
import News from "../../domain/model/entities/News";
import IAction from "./IAction";

import ActionsTypes from "../constants/ActionTypes";
import PaginationProperties from "../../domain/model/dto/PaginationProperties";
import IPaginationAction from "./IPaginationAction";
import Statistics from "../../domain/model/dto/Statistics";

export function fetchNews(channels: Channel[], pagination?: PaginationProperties) : IPaginationAction<Channel[]>{
    return {type: ActionsTypes.NEWS.FETCH_REQUEST_MANY, payload: channels, pagination};
}

export function fetchNewsSucceded(news: News[]): IAction<News[]>{
    return {type: ActionsTypes.NEWS.FETCH_SUCCEDED_MANY, payload: news};
}

export function fetchNewsById(id: string): IAction<string>{
    return {type: ActionsTypes.NEWS.FETCH_BY_ID, payload: id};
}

export function saveNews(news: News): IAction<News>{
    return {type: ActionsTypes.NEWS.SAVE_ONE, payload: news};
}

export function postNews(news: News): IAction<News>{
    return {type: ActionsTypes.NEWS.POST_REQUEST, payload: news};
}

export function deleteNewsRequest(news: News) : IAction<News>{
    return {type: ActionsTypes.NEWS.DELETE_REQUEST, payload: news};
}

export function deleteNews(news: News) : IAction<News>{
    return {type: ActionsTypes.NEWS.DELETE_ONE, payload: news};
}

export function fetchNewsStatistics(): IAction<null>{
    return {type: ActionsTypes.NEWS.FETCH_STATISTICS, payload: null};
}

export function saveNewsStatistics(statisitcs: Statistics): IAction<Statistics>{
    return {type: ActionsTypes.NEWS.SAVE_STATISTICS, payload: statisitcs};
}