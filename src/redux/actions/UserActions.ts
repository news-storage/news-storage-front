import User from "../../domain/model/entities/User";
import IAction from "./IAction";

import ActionsTypes from "../constants/ActionTypes";

export function saveUser(user: User): IAction<User>{
    return { type: ActionsTypes.USER.SAVE, payload: user };
}

export function cleanUser(): IAction<null>{
    return { type: ActionsTypes.USER.CLEAN, payload: null };
}