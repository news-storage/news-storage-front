import { Action } from "redux";

export default interface IAction<T =  any> extends Action<string> {
    payload: T;
}