
import * as UserActions from './UserActions';
import * as ChannelActions from './ChannelActions';
import * as NewsActions from './NewsAсtions';

const actions = {
    ...UserActions,
    ...ChannelActions,
    ...NewsActions
};

export default actions;