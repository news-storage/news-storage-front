import { call, put, select } from 'redux-saga/effects';

import actions from '../actions';
import IAction from '../actions/IAction';
import IPaginationAction from '../actions/IPaginationAction';

import * as NewsService from '../../domain/services/NewsService';

import Channel from '../../domain/model/entities/Channel';
import User from '../../domain/model/entities/User';
import News from '../../domain/model/entities/News';
import Statistics from '../../domain/model/dto/Statistics';

export function* fetchNews(action: IPaginationAction<Channel[]>) {
    const news: News[] = yield call(NewsService.fetchNews, action.payload, action.pagination);
    yield put(actions.fetchNewsSucceded(news));
}

export function* saveRequestNews(action: IAction<News>) {
    const user: User = yield select(state => state.user);
    const news: News = yield call(NewsService.saveNews, action.payload, user);
    yield put(actions.saveNews(news));
}

export function* deleteNewsRequest(action: IAction<News>) {
    const user: User = yield select(state => state.user);
    const news: News = yield call(NewsService.deleteNews, action.payload, user);
    yield put(actions.deleteNews(news));
}

export function* fetchNewsById(action: IAction<string>) {
    const news: News | null = yield call(NewsService.findNewsById, action.payload);
    if (news) {
        yield put(actions.saveNews(news));
    }
}

export function* fetchStatistics(action: IAction<null>){
    const stats: Statistics = yield call(NewsService.getStatistics);
    yield put(actions.saveNewsStatistics(stats));
}