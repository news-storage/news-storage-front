
import { takeEvery } from 'redux-saga/effects';

import * as channelsSaga from './channels';
import * as newsSaga from './news';

import ActionsTypes from '../constants/ActionTypes';

export default function* saga(){
    yield takeEvery(ActionsTypes.CHANNELS.FETCH_REQUEST_MANY, channelsSaga.fetchChannels);
    yield takeEvery(ActionsTypes.CHANNELS.DELETE_REQUEST_ONE, channelsSaga.fetchChannel);
    yield takeEvery(ActionsTypes.CHANNELS.SAVE_REQUEST_ONE, channelsSaga.saveRequestChannel);
    yield takeEvery(ActionsTypes.CHANNELS.DELETE_REQUEST_ONE, channelsSaga.deleteRequestChannel);
    yield takeEvery(ActionsTypes.CHANNELS.FIND_BY_NAME, channelsSaga.findChannelsByName);
    yield takeEvery(ActionsTypes.CHANNELS.FETCH_STATISTICS, channelsSaga.fetchStatistics);

    yield takeEvery(ActionsTypes.NEWS.FETCH_REQUEST_MANY, newsSaga.fetchNews);
    yield takeEvery(ActionsTypes.NEWS.FETCH_BY_ID, newsSaga.fetchNewsById);
    yield takeEvery(ActionsTypes.NEWS.POST_REQUEST, newsSaga.saveRequestNews);
    yield takeEvery(ActionsTypes.NEWS.DELETE_REQUEST, newsSaga.deleteNewsRequest);
    yield takeEvery(ActionsTypes.NEWS.FETCH_STATISTICS, newsSaga.fetchStatistics);
}