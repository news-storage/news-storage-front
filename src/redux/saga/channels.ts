import { call, put, select } from 'redux-saga/effects';

import IAction from '../actions/IAction';
import actions from '../actions';

import * as ChannelsService from "../../domain/services/ChannelsService";

import Channel from '../../domain/model/entities/Channel';
import User from '../../domain/model/entities/User';
import PaginationProperties from '../../domain/model/dto/PaginationProperties';
import Statistics from '../../domain/model/dto/Statistics';

export function* fetchChannels(action: IAction<PaginationProperties | undefined>) {
    const channels: Channel[] = yield call(ChannelsService.fetchChannels, action.payload);
    yield put(actions.fetchRequestChannelsSucceded(channels));
}

export function* fetchChannel(action: IAction<string>) {
    const channel: Channel = yield call(ChannelsService.fetchChannel, action.payload);
    yield put(actions.fetchRequestChannelSucceded(channel));
}

export function* saveRequestChannel(action: IAction<Channel>) {
    const user: User = yield select(state => state.user);
    const channel: Channel = yield call(ChannelsService.saveChannel, action.payload, user);
    yield put(actions.fetchRequestChannelSucceded(channel));
}

export function* deleteRequestChannel(action: IAction<Channel>) {
    const user: User = yield select(state => state.user);
    const channel: Channel = yield call(ChannelsService.deleteChannel, action.payload, user);
    yield put(actions.deleteChannel(channel));
}

export function* findChannelsByName(action: IAction<string>){
    const channels: Channel[] = yield call(ChannelsService.findChannelsByName, action.payload);
    yield put(actions.fetchRequestChannelsSucceded(channels));
}

export function* fetchStatistics(action: IAction<null>){
    const stats: Statistics = yield call(ChannelsService.getStatistics);
    yield put(actions.saveChannelStatistics(stats));
}
