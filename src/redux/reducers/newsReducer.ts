import News from "../../domain/model/entities/News";

import IAction from "../actions/IAction";

import ActionsTypes from "../constants/ActionTypes";

export default function reducer(state: News[] = [], action: IAction): News[] {

    switch(action.type){
        case ActionsTypes.NEWS.FETCH_SUCCEDED_MANY:
            return action.payload;
        case ActionsTypes.NEWS.SAVE_ONE:
            return save(state, action);
        case ActionsTypes.NEWS.DELETE_ONE:
            return deleteNews(state, action);
        default:
            break;
    }
    return state;
}

function save(state: News[], action: IAction<News>): News[]{
    const news = action.payload;
    const newState = state.map((value) => value);
    const index = newState.findIndex((value) => value.id === news.id);
    if(index >= 0){
        newState[index] = news;
    }else{
        newState.push(news);
    }
    return newState;
    
}

function deleteNews(state: News[], action: IAction<News>): News[]{
    const news = action.payload;
    const newState = state.map(value => value);
    const index = state.findIndex(value => value.id === news.id);
    newState.splice(index, 1);
    return newState;
}