import Channel from "../../domain/model/entities/Channel";
import IAction from "../actions/IAction";

import ActionsTypes from "../constants/ActionTypes";

export default function reducer(state: Channel[] = [], action: IAction): Channel[] {

    switch(action.type){
        case ActionsTypes.CHANNELS.FETCH_MANY_SUCCEDED:
            return fetchManySucceded(state, action);
        case ActionsTypes.CHANNELS.CHANGE_ONE:
            return changeOne(state, action);
        case ActionsTypes.CHANNELS.DELETE_ONE:
            return deleteOne(state, action);
        default:
            break;
    }
    return state;
}

function fetchManySucceded(state: Channel[], action: IAction<Channel[]>): Channel[]{
    const payload = action.payload as Channel[];
    return payload;
}

function changeOne(state: Channel[], action: IAction<Channel>): Channel[]{
    const channel = action.payload as Channel;
    const newChannels = state.map(value => value);
    const index = state.findIndex(value => value.id === channel.id);
    if(index === -1){
            newChannels.push(channel);
    }else{
        newChannels[index] = channel;
    }
    return newChannels;
}

function deleteOne(state: Channel[], action: IAction<Channel>): Channel[]{
    const channel = action.payload as Channel;
    const newChannels = state.map(value => value);
    const index = state.findIndex(value => value.id === channel.id);
    newChannels.splice(index, 1);
    return newChannels;
}