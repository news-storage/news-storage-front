import User from '../../domain/model/entities/User';
import IAction from '../actions/IAction';

import ActionsTypes from "../constants/ActionTypes";

export default function reducer(state: User | null = null, action: IAction<User | null>): User | null {

    switch(action.type){
        case ActionsTypes.USER.SAVE:
            return saveUser(state, action);
        case ActionsTypes.USER.CLEAN:
            return cleanUser(state, action);
        default:
            break;
    }
    return state;
}

function saveUser(state: User | null = null, action: IAction<User | null>): User {
    const payload = action.payload as User;
    return {...payload};
}

function cleanUser(state: User | null = null, action: IAction){
    return null;
}