import { combineReducers } from 'redux';

import channelsReducer from './channelsReducer';
import newsReducer from './newsReducer';
import userReducer from './userReducer';
import statisticsChannels from './statisticsChannels';
import statisticsNews  from './statisticsNews';

export default combineReducers({ 
    user:  userReducer,
    channels: channelsReducer,
    news: newsReducer,
    statisticsChannels: statisticsChannels,
    statisticsNews: statisticsNews,
 });