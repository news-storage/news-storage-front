import Statistics from "../../domain/model/dto/Statistics";

import IAction from "../actions/IAction";
import ActionsTypes from "../constants/ActionTypes";

export default function reducer(state: Statistics = {count: 0}, action: IAction): Statistics {

    switch(action.type){
        case ActionsTypes.CHANNELS.SAVE_STATISTICS:
            return save(state, action);
        default:
            break;
    }
    return state;
}

function save(state: Statistics, action: IAction<Statistics>): Statistics {
    return action.payload;
}