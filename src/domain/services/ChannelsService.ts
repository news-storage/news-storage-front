import axios from 'axios';
import { BACKEND_URL } from '../../lib/constants';
import PaginationProperties from '../model/dto/PaginationProperties';
import Statistics from '../model/dto/Statistics';
import Channel from '../model/entities/Channel';
import User from '../model/entities/User';


export async function fetchChannels(pagination?: PaginationProperties): Promise<Channel[]> {
    try {
        const response = await axios.get(`${BACKEND_URL}/channels`, { params: pagination });
        return response.data as Channel[];
    } catch (error) {
        console.error(error);
        return [];
    }
}

export async function fetchChannel(id: string): Promise<Channel | null> {
    try {
        const response = await axios.get(`${BACKEND_URL}/channels/${id}`);
        return response.data as Channel;
    } catch (error) {
        console.error(error);
        return null;
    };
}

export async function saveChannel(channel: Channel, user: User): Promise<Channel> {
    try {
        const method = (channel.id) ? 'put' : 'post';
        const response = await axios({ auth: user, method, url: `${BACKEND_URL}/channels/`, data: channel });
        if (response.status === 200) {
            return response.data as Channel;
        } else {
            return channel;
        }
    } catch (error) {
        console.error(error);
        return channel;
    }
}

export async function deleteChannel(channel: Channel, user: User): Promise<Channel> {
    try {
        await axios.delete(`${BACKEND_URL}/channels/${channel.id}`, { auth: user });
        return channel;
    } catch (error) {
        console.error(error);
        return channel;
    }
}

export async function findChannelsByName(name: string): Promise<Channel[]> {
    try {
        const response = await axios.get(`${BACKEND_URL}/channels/?name=${name}`);
        return response.data as Channel[];
    } catch (error) {
        console.error(error);
        return [];
    }
}

export async function getStatistics(): Promise<Statistics> {
    try {
        const response = await axios.get(`${BACKEND_URL}/channels/statistics`);
        if(response.status === 200){
            return response.data as Statistics;
        }
        return {count: 0};
    }catch(error){
        console.error(error);
        return {count: 0};
    }
}
