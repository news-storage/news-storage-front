import axios from 'axios';
import { BACKEND_URL } from '../../lib/constants';
import User from '../model/entities/User';


export async function checkUser(user: User): Promise<boolean> {
    try {
        const response = await axios.post(`${BACKEND_URL}/users/check`, user);
        return (response.status === 200);
    } catch (error) {
        console.error(error);
        return false;
    }
}
