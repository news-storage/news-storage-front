import axios from 'axios';

import {BACKEND_URL} from '../../lib/constants';
import PaginationProperties from '../model/dto/PaginationProperties';
import Statistics from '../model/dto/Statistics';

import Channel from '../model/entities/Channel';
import News from '../model/entities/News';
import User from '../model/entities/User';

import {getCreateNewsRequest} from './DataTransofrmation';

export async function fetchNews(channels: Channel[], pagination?: PaginationProperties): Promise<News[]> {
    try{
        const channnelsParam = channels.map((value) => `channels=${value.id}`).join('&');
        const response = await axios.get(`${BACKEND_URL}/news/?${channnelsParam}`, { params: pagination });
        return response.data as News[];
    }catch(error){
        console.error(error);
        return [];
    }
}

export async function findNewsById(id: string): Promise<News | null> {
    try{
        const response = await axios.get(`${BACKEND_URL}/news/${id}`);
        return response.data as News;
    }catch(error){
        console.error(error);
        return null;
    }
}

export async function saveNews(news: News, user: User): Promise<News> {
    try{
        const data = getCreateNewsRequest(news);
        const method = (news.id) ? 'put': 'post';
        const response = await axios({auth: user, method, url: `${BACKEND_URL}/news/`, data});
        if(response.status === 200){
            return response.data as News;
        }else{
            return news;
        }
    }catch(error){
        console.error(error);
        return news;
    }
}

export async function deleteNews(news: News, user: User): Promise<News> {
    try{
        await axios.delete(`${BACKEND_URL}/news/${news.id}`, {auth: user});
        return news;
    }catch(error){
        console.error(error);
        return news;
    }
}

export async function getStatistics(): Promise<Statistics> {
    try {
        const response = await axios.get(`${BACKEND_URL}/news/statistics`);
        if(response.status === 200){
            return response.data as Statistics;
        }
        return {count: 0};
    }catch(error){
        console.error(error);
        return {count: 0};
    }
}