import CreateNewsRequest from "../model/dto/CreateNewsRequest";
import News from "../model/entities/News";

export function getCreateNewsRequest(news: News): CreateNewsRequest{
    return {
        id: news.id,
        caption: news.caption,
        text: news.text,
        postingDate: news.postingDate,
        finalDate: news.finalDate,
        channels: news.channels.map((value) => value.id as string)
    }
}