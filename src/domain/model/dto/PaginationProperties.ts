export default interface PaginationProperties {
    skip?: number;
    count?: number;
}