interface CreateNewsRequest {
    id?: string;
    caption: string;
    text: string;
    channels: string[];
    postingDate?: Date;
    finalDate: Date;
}

export default CreateNewsRequest;