import Channel from "./Channel";
import Entity from "./IEntity";

export default interface News extends Entity {
    caption: string;
    text: string;
    postingDate: Date;
    finalDate: Date;
    channels: Channel[];
}