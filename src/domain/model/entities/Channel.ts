import IEntity from "./IEntity";

export default interface Channel extends IEntity {
    name: string;
}