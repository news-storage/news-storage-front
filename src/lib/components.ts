export function classVisibility(isVisible: boolean): "visible" | "invisible"{
    const result = isVisible ? "visible": "invisible";
    return result;
}