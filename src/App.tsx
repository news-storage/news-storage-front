import AppRouter from './components/routing/AppRouter';

import 'bootstrap/dist/css/bootstrap.min.css';

import {
  BrowserRouter as Router
} from "react-router-dom";

function App() {
  return (
    <Router>
      <AppRouter />
    </Router>
  );
}

export default App;
